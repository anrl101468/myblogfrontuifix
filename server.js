const http = require("http");
const express = require("express");
const path = require("path");
const compression = require('compression');
const xmlparser = require('express-xml-bodyparser');
const app = express();
const robots = require('express-robots-txt');
const { createProxyMiddleware } = require('http-proxy-middleware');
const { spawnSync } = require('child_process');

const proxy = require('express-http-proxy');
const cors = require('cors');
function getHostIpAddress() {
  const command = 'ip route | grep default | awk \'{ print $3; }\'';

  const { stdout } = spawnSync(command, { shell: true });
  if (stdout) {
    return `http://${stdout.toString().trim()}:8080`;
    // return `http://localhost:8095/`;
  }

  return null;
}

const port = 3000; // 인스턴스 생성시 만들었던 포트번호 기입

const API_SERVER_URL = getHostIpAddress();
const LISTEN_IP = "0.0.0.0"; // Express 서버가 수신할 IP 주소

app.use(compression());
app.use(xmlparser());
app.use(robots(path.join(__dirname+'/public/robots.txt')));
app.get("/ping", (req, res) => {
  res.send("pong");
});

// app.use('/api', proxy(API_SERVER_URL, {
//   proxyReqPathResolver: (req) => `/api${req.url}`
// }));

app.use('/api', createProxyMiddleware({ target: API_SERVER_URL, changeOrigin: true }));

app.use(
  express.static(path.join(__dirname, "dist")),
  cors({
    origin: true,
    credentials: true,
  }),
);

app.use(function(req, res, next) {
  res.setHeader('Cache-Control', 'max-age=31536000');
  next();
});
app.get('/sitemap.xml', (req, res) => {
  res.sendFile(path.join(__dirname+'/public/sitemap.xml'));
});

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname+'/dist/index.html'));
});

http.createServer(app).listen(port, LISTEN_IP, () => {
  console.log(`app listening at ${LISTEN_IP}:${port}`);
});