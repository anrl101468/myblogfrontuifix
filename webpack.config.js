const path = require('path');
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;
const mode = process.env.NODE_ENV || 'production'; // NODE_ENV 환경 변수가 없는 경우 기본값으로 production 모드를 사용합니다.

module.exports = {
  mode: mode, // NODE_ENV 환경 변수가 없는 경우 기본값으로 production 모드를 사용합니다.,
  entry: {
    main : path.resolve(__dirname, 'src/index.js'), // entry 경로 설정
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  performance: {
    hints: mode === "production"? false : "warning",
    maxEntrypointSize: 124400000,
    maxAssetSize: 12440000
  },
  optimization: {
    minimize: true,
    minimizer: [
      new CssMinimizerPlugin(), // CSS 압축
      new TerserPlugin({ // JS 난독화 등
        terserOptions: {
          compress: {
            drop_console: true // console.log 제거
          }
        }
      })
    ],
    splitChunks: {
      chunks: 'all',    // include all types of chunks
      maxSize: 244000,
    },
  },
  devtool:  mode === 'development' && 'eval-cheap-module-source-map',
  devServer: {
    static: {
      directory: path.join(__dirname, 'public'),
    },
    port: 3000,
    historyApiFallback: true,
    open: true,
    hot:true,
    compress:true,
    headers:{'Cache-Control': 'max-age=31536000'},
    client: {
      overlay: true,
      // 웹소켓용 url 지정
      webSocketURL: "ws://0.0.0.0:80/ws",
    },
    proxy: {
      '/api': {
        target: 'http://192.168.0.114:8080',
        changeOrigin: true,
      },
    },
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'esbuild-loader',
          options: {
            loader: 'jsx', // .js 파일 내에서 JSX를 지원합니다.
            target: 'es2015'
          },
        },
      },
      {
        test: /\.(png|jpe?g|gif|svg|ico|webp)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[path][name].[ext]',
            },
          },
        ],
      },
      /*{
        test: /\.(png|jpg|jpeg|gif|webp)$/i,
        loader: 'image-webpack-loader',
        options: {
          name: '[path][name].[ext]',
        },
        enforce: 'pre',
      },*/
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.xml$/,
        use: 'xml-loader'
      }
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'public/index.html',
    }),
    new CleanWebpackPlugin(),
    /*new BundleAnalyzerPlugin(),*/
  ].filter(Boolean),
  resolve: {
    modules: [path.resolve(__dirname, 'src'), 'node_modules'],
    extensions: [".js", ".jsx"],
  },
}