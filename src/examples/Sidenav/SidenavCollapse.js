/*!

=========================================================
* Vision UI Free React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/vision-ui-free-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com/)
* Licensed under MIT (https://github.com/creativetimofficial/vision-ui-free-react/blob/master LICENSE.md)

* Design and Coded by Simmmple & Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

// prop-types is a library for typechecking of props.
import PropTypes from "prop-types";

// @mui material components
import Collapse from "@mui/material/Collapse";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Icon from "@mui/material/Icon";

// Vision UI Dashboard React components
import VuiBox from "components/VuiBox";

// Custom styles for the SidenavCollapse
import {
  collapseItem,
  collapseIconBox,
  collapseIcon,
  collapseText,
} from "examples/Sidenav/styles/sidenavCollapse";

// Vision UI Dashboard React context
import { useVisionUIController } from "context";
import Menu from "@mui/material/Menu";
import { Link, NavLink } from "react-router-dom";
import MenuItem from "@mui/material/MenuItem";
import React, { useEffect, useRef, useState } from "react";
import { observer } from "mobx-react";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import IconButton from "@mui/material/IconButton";
import { MenuModal } from "../../components/MyMenu/MenuModal";
import { useStores } from "../../state/context/Context";
import { v4 as uuidv4 } from "uuid";
import ArrowDropDownCircleSharpIcon from "@mui/icons-material/ArrowDropDownCircleSharp";
import UpdateIcon from "@mui/icons-material/Update";
import qs from "qs";
import VuiTypography from "../../components/VuiTypography";

const SidenavCollapse = observer(({ color, icon, modifyIcon, clearIcon, name, menuIdx ,index,  subMenus ,active,useComponent, ...rest }) => {
  const [controller] = useVisionUIController();
  const { miniSidenav, transparentSidenav } = controller;
  const {menuModalStore,menuStore,usrStore} = useStores();
  const divRef = React.useRef();
  const [open,setOpen] = useState(false);
  const [anchorEl,setAnchorEl] = useState(null);

  useEffect(() => {
      if (useComponent === "myMenu" && divRef.current && menuIdx === menuStore.recentCreateMenuIdx)
      {
        setAnchorEl(divRef.current); // 마운트 이후에 divRef.current 값을 할당합니다.
        setOpen(true);
      }
  }, []);

  const handleClick = () =>
  {
    setAnchorEl(divRef.current);
    setOpen(true);
  }

  const handleClose = () =>
  {
    setAnchorEl(null);
    setOpen(false);
    menuStore.setRecentCreateMenuIdx(null);
  }

  const handleAddMenuOpen = () =>
  {
    menuModalStore.setMenuTitle('');
    menuModalStore.setMenuIdx(menuIdx);
    menuModalStore.setAddOpen(true);
  }

  const modifyOpenClick = (modalTitle,modalIdx) =>
  {
    menuModalStore.setMenuTitle(modalTitle);
    menuModalStore.setMenuIdx(modalIdx);
    menuModalStore.setModifyOpen(true);
  }
  
  const confirmCheck = (message,targetIdx) =>
  {
    if(window.confirm(message))
    {
      menuStore.deleteMenu(targetIdx);
    }
    else{
      window.alert("취소되었습니다.");
    }
  }

  const renderSubMenu = subMenus.map( (x,i) => {
    if(useComponent === "nav")
    {
      return (
          <Link to={`/user/${usrStore.usrId}/${x.menuName}`} key={uuidv4()}>
            <MenuItem key={uuidv4()} onClick={handleClose}>
                {x.menuName}
            </MenuItem>
          </Link>
      )
    }
    else
    {
      return (
        <MenuItem key={uuidv4()} onClick={handleClose}>
          <Link to={`/user/${usrStore.usrId}/${x.menuName}`} key={uuidv4()}>
            <VuiTypography>
              {x.menuName}
            </VuiTypography>
          </Link>
          <ListItemIcon
            key={uuidv4()}
            onClick={() => modifyOpenClick(x.menuName,x.menuIdx)}
            sx={{ml:2}}
          >
            <Icon sx={(theme) => collapseIcon(theme, { active })}>{modifyIcon}</Icon>
          </ListItemIcon>
          <ListItemIcon
            key={uuidv4()}
            onClick={() => confirmCheck("삭제하시겠습니까?",x.menuIdx)}
          >
            <Icon sx={(theme) => collapseIcon(theme, { active })}>{clearIcon}</Icon>
          </ListItemIcon>
        </MenuItem>
      )
    }
  });

  return (
    <>
      <ListItem component="li">
        <VuiBox {...rest} sx={(theme) => collapseItem(theme, { active, transparentSidenav })}
                onClick={handleClick}
        >
          <ListItemIcon
            sx={(theme) => collapseIconBox(theme, { active, transparentSidenav, color })}
          >
            {typeof icon === "string" ? (
              <Icon sx={(theme) => collapseIcon(theme, { active })}>{icon}</Icon>
            ) : (
              icon
            )}
          </ListItemIcon>
          <ListItemText
            primary={name}
            sx={(theme) => collapseText(theme, { miniSidenav, transparentSidenav, active })}
            ref={divRef}
          />
          {
            useComponent === "myMenu" && (
              <>
                <ListItemIcon
                key={uuidv4()}
                onClick={ () => modifyOpenClick(name,menuIdx)}
                >
                <Icon sx={(theme) => collapseIcon(theme, { active })}>{modifyIcon}</Icon>
                </ListItemIcon>
                <ListItemIcon
                key={uuidv4()}
                onClick={() => confirmCheck("삭제하시겠습니까?",menuIdx)}
                >
                <Icon sx={(theme) => collapseIcon(theme, { active })}>{clearIcon}</Icon>
                </ListItemIcon>
              </>
            )
          }

        </VuiBox>
      </ListItem>
      <Menu
        open={open}
        onClose={handleClose}
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: "top",
          horizontal: "left"
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "left"
        }}
        style={{ marginLeft: "30px" }}
      >
        {renderSubMenu && (
          [...renderSubMenu,
            useComponent === "nav" ? null : (
              <IconButton  key={uuidv4()} onClick={handleAddMenuOpen}>
                <AddCircleOutlineIcon/>
              </IconButton>
            )
          ]
        )}
      </Menu>
    </>
  );
})

// Setting default values for the props of SidenavCollapse
SidenavCollapse.defaultProps = {
  color: "info",
  active: false,
  children: false,
  open: false,
};

// Typechecking props for the SidenavCollapse
SidenavCollapse.propTypes = {
  color: PropTypes.oneOf(["info", "success", "warning", "error", "dark"]),
  icon: PropTypes.node.isRequired,
  name: PropTypes.string.isRequired,
  children: PropTypes.node,
  active: PropTypes.bool,
  open: PropTypes.bool,
};

export default SidenavCollapse;
