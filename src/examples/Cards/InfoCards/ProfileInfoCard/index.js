/*!

=========================================================
* BLOG Free React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/vision-ui-free-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com/)
* Licensed under MIT (https://github.com/creativetimofficial/vision-ui-free-react/blob/master LICENSE.md)

* Design and Coded by Simmmple & Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

// prop-types is library for typechecking of props
import PropTypes from "prop-types";

// @mui material components
import Card from "@mui/material/Card";
import Divider from "@mui/material/Divider";

// BLOG Dashboard React components
import VuiBox from "components/VuiBox";
import VuiTypography from "components/VuiTypography";

// BLOG Dashboard React base styles
import typography from "assets/theme/base/typography";
import VuiInput from "../../../../components/VuiInput";
import { useStores } from "../../../../state/context/Context";
import React, { useEffect, useState } from "react";
function ProfileInfoCard({ title, description, info }) {
  const [des,setDes] = useState(description);
  const labels = [];
  const values = [];
  const { size } = typography;
  const {usrStore} = useStores();


  // Convert this form `objectKey` of the object key in to this `object key`
  Object.keys(info).forEach((el) => {
    if (el.match(/[A-Z\s]+/)) {
      const uppercaseLetter = Array.from(el).find((i) => i.match(/[A-Z]+/));
      const newElement = el.replace(uppercaseLetter, ` ${uppercaseLetter.toLowerCase()}`);

      labels.push(newElement);
    } else {
      labels.push(el);
    }
  });

  // Push the object values into the values array
  Object.values(info).forEach((el) => values.push(el));

  // Render the card info items
  const renderItems = labels.map((label, key) => (
    <VuiBox key={label} display="flex" py={1} pr={2}>
      <VuiTypography variant="button" color="text" fontWeight="regular" textTransform="capitalize">
        {label}: &nbsp;
      </VuiTypography>
      <VuiTypography variant="button" fontWeight="regular" color="white">
        &nbsp;{values[key]}
      </VuiTypography>
    </VuiBox>
  ));

  return (
    <Card
      sx={{
        height: "100%",
      }}
    >
      <VuiBox display="flex" mb="14px" justifyContent="space-between" alignItems="center">
        <VuiTypography variant="lg" fontWeight="bold" color="white" textTransform="capitalize">
          {title}
        </VuiTypography>
      </VuiBox>
      <VuiBox>
        <VuiBox mb={2} lineHeight={1}>
          {/*<VuiTypography variant="button"
                         color="text"
                         fontWeight="regular"
          >
            {description}
          </VuiTypography>*/}
          <VuiInput value={des}
                    component="text"
                    placeholder="소개글을 등록해 보세요.(글을 작성후 저장하실려면 엔터를 눌러주세요)"
                    onClick={
                      (event) => {
                        if(usrStore.usr.usrGt === null) {
                          event.target.value = '';
                        }
                      }
                    }
                    onChange={(event) => {
                      setDes(event.target.value)
                    }}
                    onKeyPress={
                      (event) => {
                        if(event.key === 'Enter') {
                          if(des.length === 0)
                          {
                            window.alert("등록할 자기소개를 입력해주세요");
                            return false;
                          }
                          usrStore.updateUsr("usrGt",des);
                        }
                      }
                    }
          />
        </VuiBox>
        <VuiBox opacity={0.3}>
          <Divider/>
        </VuiBox>
        <VuiBox>
          {renderItems}
        </VuiBox>
      </VuiBox>
    </Card>
  );
}

// Typechecking props for the ProfileInfoCard
ProfileInfoCard.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  info: PropTypes.objectOf(PropTypes.string).isRequired,
};

export default ProfileInfoCard;
