/*!

=========================================================
* BLOG Free React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/vision-ui-free-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com/)
* Licensed under MIT (https://github.com/creativetimofficial/vision-ui-free-react/blob/master LICENSE.md)

* Design and Coded by Simmmple & Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

// @mui material components
import Card from "@mui/material/Card";

// BLOG Dashboard React components
import VuiBox from "components/VuiBox";
import VuiTypography from "components/VuiTypography";
// BLOG Dashboard React example components
import Table from "examples/Tables/Table";
// Data
import { Link, NavLink, Redirect, useLocation, useParams } from "react-router-dom";
import { observer } from "mobx-react";
import { useEffect, useRef, useState } from "react";
import React from "react";
import { v4 as uuidv4 } from "uuid";
import { useHistory } from "react-router-dom/cjs/react-router-dom";
import VuiInput from "components/VuiInput";
import { Meta } from "Meta";
import { useStores } from "../../../state/context/Context";

const DashboardTables = observer((props) => {

  const {postStore,usrStore,postPagingStore,menuStore} = useStores();
  const history = useHistory();
  const params = useParams();

  let rows = [];

  const columns= [
    { name: "제목", align: "center" , width: "70%"},
    { name: "작성일", align: "center" , width: "15%"},
    { name: "조회수", align: "center" , width: "15%"},
  ];

  if (postStore.posts.length > 0) {
    rows = postStore.posts
      rows = rows.map((x,i) => ({
        제목: (
          <NavLink key={uuidv4()} to={`/user/${params.usrId}/${params.menuName}/${x.uplIdx}`}>
            <VuiTypography variant="caption" fontWeight="medium" color="white">
              {x.uplTitle}
            </VuiTypography>
          </NavLink>
        ),
        작성일: (
          <VuiTypography variant="caption" color="white" fontWeight="medium" key={uuidv4()}>
            {x.uplRegDt.substring(0,16)}
          </VuiTypography>
        ),
        조회수: (
          <VuiTypography variant="caption" color="white" fontWeight="medium" key={uuidv4()}>
            {x.uplVcnt}
          </VuiTypography>
        ),
      }));
  }else {
    rows = [
      {
        제목: (
          <VuiTypography variant="caption" fontWeight="medium" color="white" key={uuidv4()}>
            등록된 글이 없습니다.
          </VuiTypography>
        ),
        작성일: (
          <></>
        ),
        조회수: (
          <></>
        ),
      }
    ]
  }


  return (
      <VuiBox py={3}>
          <Card>
            <VuiTypography>{props.name}</VuiTypography>
            <VuiBox
              sx={{
                "& th": {
                  borderBottom: ({ borders: { borderWidth }, palette: { grey } }) =>
                    `${borderWidth[1]} solid ${grey[700]}`,
                },
                "& .MuiTableRow-root:not(:last-child)": {
                  "& td": {
                    borderBottom: ({ borders: { borderWidth }, palette: { grey } }) =>
                      `${borderWidth[1]} solid ${grey[700]}`,
                  }
                }
            }}
            >
              <Table columns={columns} rows={rows} />
            </VuiBox>
          </Card>
      </VuiBox>
  );
});

export default DashboardTables;
