import DashboardNavbar from "../../examples/Navbars/DashboardNavbar";
import DashboardLayout from "../../examples/LayoutContainers/DashboardLayout";
import React, { useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import { useStores } from "../../state/context/Context";
import { useHistory } from "react-router-dom/cjs/react-router-dom";
import { Meta } from "../../Meta";
import DashboardTables from "./table";
import VuiBox from "../../components/VuiBox";
const Base = () =>
{
  const params = useParams();
  const {menuStore,usrStore,postStore,postPagingStore,instance} = useStores();
  const history = useHistory();
  useEffect(() => {
    if(!usrStore.usr)
    {
      instance.post("/api/connect");
    }
  }, []);

  useEffect(() => {
    if(usrStore.usr && usrStore.usr.usrId === params.usrId ||usrStore.usr && !params.usrId)
    {
      usrStore.setUsrId(usrStore.usr.usrId);
      menuStore.getMainMenuServerInfo(usrStore.usr.usrId);
      return ;
    }

    if(!params.usrId)
    {
      return ;
    }

    if(usrStore.usrId !== params.usrId)
    {
      menuStore.getMainMenuServerInfo(params.usrId)
        .then((res) =>{
          if(menuStore.menus.length === 0)
          {
            window.alert("등록되지 않은 ID이거나 등록된 메뉴가 없습니다.");
            history.goBack();
          }
          else
          {
              usrStore.setUsrId(params.usrId);
          }
        })
    }
  }, [usrStore.usr, params.usrId]);


  return (
    <DashboardLayout>
      <Meta
        type="web"
        description="MainPage"
        url={window.location.href}
        title="myBlog"
        siteNm="myBlog"
      />
      <DashboardNavbar />
      <VuiBox style={{display:'flex' , justifyContent:'space-around'}}>
          <VuiBox style={{width : '45%'}}>
            <DashboardTables name="일일 인기글"/>
            <DashboardTables name="일일 인기계정"/>
          </VuiBox>
          <VuiBox style={{width : '45%'}}>
            <DashboardTables name="인기글"/>
            <DashboardTables name="인기계정"/>
          </VuiBox>
      </VuiBox>
    </DashboardLayout>
    )
}

export default Base;