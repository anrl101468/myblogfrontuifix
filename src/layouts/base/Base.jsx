import DashboardNavbar from "../../examples/Navbars/DashboardNavbar";
import DashboardLayout from "../../examples/LayoutContainers/DashboardLayout";
import React, { useEffect } from "react";
import VuiBox from "../../components/VuiBox";
import Card from "@mui/material/Card";
import VuiTypography from "../../components/VuiTypography";
import { Link, useParams } from "react-router-dom";
import { useStores } from "../../state/context/Context";
import { useHistory } from "react-router-dom/cjs/react-router-dom";
import { Meta } from "../../Meta";
const Base = () =>
{
  const params = useParams();
  const {menuStore,usrStore,postStore,postPagingStore,instance} = useStores();
  const history = useHistory();
  useEffect(() => {
    if(!usrStore.usr)
    {
      instance.post("/api/connect");
    }
  }, []);

  useEffect(() => {
    if(usrStore.usr && usrStore.usr.usrId === params.usrId ||usrStore.usr && !params.usrId)
    {
      usrStore.setUsrId(usrStore.usr.usrId);
      menuStore.getMainMenuServerInfo(usrStore.usr.usrId);
      return ;
    }

    if(!params.usrId)
    {
      return ;
    }

    if(usrStore.usrId !== params.usrId)
    {
      menuStore.getMainMenuServerInfo(params.usrId)
        .then((res) =>{
          if(menuStore.menus.length === 0)
          {
            window.alert("등록되지 않은 ID이거나 등록된 메뉴가 없습니다.");
            history.goBack();
          }
          else
          {
            usrStore.setUsrId(params.usrId);
          }
        })
    }
  }, [usrStore.usr, params.usrId]);


  return (
    <DashboardLayout>
      <Meta
        type="web"
        description="설명 페이지 입니다."
        url={window.location.href}
        title="myBlog"
        siteNm="myBlog"
      />
      <DashboardNavbar />
      <VuiBox>
        <Card>
          <VuiTypography color="white" fontWeight="bold" fontSize="40px">
            사용법
          </VuiTypography>
        </Card>
        <Card sx={{mt:'1rem'}}>
          <VuiTypography  color="white" fontWeight="bold" fontSize="24px">
            1. 로그인 -> 회원가입을통해 회원가입 할수 있습니다<Link to="/authentication/sign-up">(바로가기)</Link>
          </VuiTypography>
          <VuiTypography  color="white" fontWeight="bold" fontSize="24px" sx={{mt:'1rem'}}>
            2. login후 profile 이동후 (+)버튼 클릭 -> 메인 메뉴 등록
          </VuiTypography>
          <VuiTypography  color="white" fontWeight="bold" fontSize="24px" sx={{mt:'1rem'}}>
            3. 메인 메뉴 등록 후 나오는 상자안의  (+)버튼 클릭 -> 서브 메뉴 등록
          </VuiTypography>
          <VuiTypography color="white" fontWeight="bold" fontSize="24px" sx={{mt:'1rem'}}>
            4. 화면 좌측 사이드바에 해당하는 서브 메뉴 혹은 메뉴세팅의 서브메뉴 클릭 -> 글 등록
          </VuiTypography>
        </Card>
      </VuiBox>
      <VuiBox sx={{mt:'1rem'}}>
        <Card>
          <VuiTypography color="white" fontWeight="bold" fontSize="40px">
            기능 설명
          </VuiTypography>
        </Card>
        <Card sx={{mt:'1rem'}}>
          <VuiTypography  color="white" fontWeight="bold" fontSize="14px" sx={{mt:'1rem'}}>
            1. Oauth를 통한 로그인
          </VuiTypography>
          <VuiTypography  color="white" fontWeight="bold" fontSize="14px" sx={{mt:'1rem'}}>
            2. 능동적인 메뉴구성
          </VuiTypography>
          <VuiTypography  color="white" fontWeight="bold" fontSize="14px" sx={{mt:'1rem'}}>
            3. ckEditor를 활용한 글 작성페이지
          </VuiTypography>
          <VuiTypography  color="white" fontWeight="bold" fontSize="14px" sx={{mt:'1rem'}}>
            4. 댓글 기능
          </VuiTypography>
          <VuiTypography  color="white" fontWeight="bold" fontSize="14px" sx={{mt:'1rem'}}>
            5. 다른사람의 블로그 방문 기능
          </VuiTypography>
        </Card>
      </VuiBox>
    </DashboardLayout>
  )
}

export default Base;