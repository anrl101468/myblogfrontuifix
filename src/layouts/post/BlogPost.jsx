import DashboardNavbar from "../../examples/Navbars/DashboardNavbar";
import React, { useEffect, useRef, useState ,lazy} from "react";
import DashboardLayout from "../../examples/LayoutContainers/DashboardLayout";

import { CKEditor } from "@ckeditor/ckeditor5-react";

import VuiBox from "../../components/VuiBox";
import Card from "@mui/material/Card";
import VuiInput from "../../components/VuiInput";
import VuiButton from "../../components/VuiButton";
import { useLocation, useParams } from "react-router-dom";
import { useStores } from "../../state/context/Context";
import { observer } from "mobx-react";
import { useHistory } from "react-router-dom/cjs/react-router-dom";
import { v4 as uuidv4 } from "uuid";
import TimelineList from "../../examples/Timeline/TimelineList";
import VuiTypography from "../../components/VuiTypography";
import { CkeditorConfig } from "./CkeditorConfig";
import ClassicEditor from 'ckeditor';
import { Meta } from "../../Meta";
import MyCkeditor from "../../components/MyCkeditor/MyCkeditor";
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import { string } from "prop-types";

const BlogPost = observer(() =>
{
  const {menuStore,usrStore,postStore,postPagingStore} = useStores();
  const params = useParams();
  const location = useLocation();
  const [mode,setMode] = useState("view");
  const [title,setTitle] = useState("");
  const [content,setContent] = useState("");
  const [uclContent,setUclContent] = useState("");
  const [uclNickname,setUclNickname] = useState("");
  const [uclPw,setUclPw] = useState("");
  const [menuIdx,setMenuIdx] = useState();
  const [menuIndex,setMenuIndex] = useState();
  const [menuSubIdx,setSubMenuIdx] = useState();
  const [menuSubIndex,setSubMenuIndex] = useState();
  const history = useHistory();
  const mounted = useRef(false);
  useEffect(()=>{
    if(params.usrId && menuStore.menus.length === 0 || params.usrId && usrStore.usrId !== params.usrId)
    {
      menuStore.getMainMenuServerInfo(params.usrId)
        .then((res) =>{
          if(menuStore.menus.length === 0)
          {
            window.alert("등록되지 않은 ID이거나 등록된 메뉴가 없습니다.");
            history.goBack();
          }
          else
          {
              usrStore.setUsrId(params.usrId);
          }
        })
    }
  },[])
  useEffect(() => {

    if(!params.uplIdx)
    {
      setMode("write");
    }

  }, [ mode,  setMode]);

  useEffect(() => {
    if(params.uplIdx)
    {
      getPost()
    }
  },[params.uplIdx])


  let tmepMenuIdx = -1;
  menuStore.menus.some((menu, menuIndex) => { // some() 메소드는 조건을 만족하는 요소를 찾으면 동작을 멈춥니다.
    const subMenuIndex = menu.subMenus.findIndex(subMenu => {
      return subMenu.menuName === params.menuName;
    });
    if (subMenuIndex >= 0) {
      if(!menuIdx)
      {
        setMenuIdx(menuStore.menus[menuIndex].menuIdx);
        setMenuIndex(menuIndex);
      }
      if(!menuSubIdx)
      {
        setSubMenuIdx(menuStore.menus[menuIndex].subMenus[subMenuIndex].menuIdx);
        setSubMenuIndex(subMenuIndex);
      }
      tmepMenuIdx = menuStore.menus[menuIndex].subMenus[subMenuIndex].menuIdx
      return true; // some() 메소드는 조건을 만족하는 요소를 찾으면 true를 반환하여 동작을 멈춥니다.
    }
  });
  if(postStore.menuIdx !== tmepMenuIdx && tmepMenuIdx !== -1)
  {
    postStore.setMenuIdx(tmepMenuIdx);
  }

  const handleMenuIdxChange = (event) => {
    setMenuIdx(event.target.value);
  };

  const handleSubMenuIdxChange = (event) => {
    setSubMenuIdx(event.target.value);
  };

  const getPost = () =>
  {
    const data = {
      menuIdx : postStore.menuIdx,
      usrId : params.usrId,
      uplIdx : params.uplIdx
    }
    postStore.selectPost(data).then(()=>{
      if(postStore.posts[0])
      {
        postStore.posts[0].selectComment();
        postStore.posts[0].updateVcnt();
        setTitle(postStore.posts[0].uplTitle);
      }
      else{
        window.alert("잘못된 접근입니다.");
        history.goBack();
      }
    })
  }

  const confirmCheck = (message,fn) => {
    if(window.confirm(message))
    {
      fn();
    }else{
      window.alert("취소하셧습니다.");
    }
  }

  const renderCommnets = (item) =>
  {
    return (
      <VuiBox key={uuidv4()} display="flex" sx={{mb:'10px'}}>
        <VuiInput sx={{maxWidth:'15%',mr:'5px'}} disabled={true} value={item.uclNickname}/>
        <VuiInput disabled={true} value={item.uclContent}
                  component="TextField"
                  multiline/>
        <VuiBox sx={{ml:'1rem',display :'flex', alignItems:'center',minWidth:'8%'}}>
          <VuiTypography fontWeight={false} sx={{fontSize:'12px',textAlign:'center'}}>
            {(item.uclRegDt).substring(0,16)}
          </VuiTypography>
        </VuiBox>
      </VuiBox>
    )
  }
  return (
    <DashboardLayout>
      <Meta
        type="web"
        url={window.location.href}
        title={title}
        description={`${usrStore.usrId}님의 블로그입니다.`}
        siteNm="myBlog"
      />
      <DashboardNavbar />
      <VuiBox
        sx={{
          ".ck-editor__editable" : {
            minHeight:'400px',
            /*maxHeight:'700px',*/
          },
          ".ck.ck-editor__main>.ck-editor__editable" : {
            backgroundColor:'#080d2c',
            color:'#a0aec0',
            borderColor:'#080d2c',
          },
          /*".ck.ck-editor__main>.ck-editor__editable:(.ck-focused)" :{
          },
          ".ck.ck-editor__main>.ck-editor__editable:not(.ck-focused)" :{
            backgroundColor:'#080d2c',
          },*/
          ".ck-toolbar" : {
            backgroundColor:'#080d2c',
            color:'#a0aec0',
            borderColor:'#080d2c',
          },
          ".ck-content pre code" : {
            color:'darkgoldenrod',
          }
        }}
      >
        <Card>
          {
            mode === "view" && postStore.posts[0] &&(
              <>
              {
                  usrStore.usr && usrStore.usr.usrId === params.usrId && (
                  <VuiBox display='flex' sx={{mb:'1rem',justifyContent:'flex-end'}}>
                    <VuiButton color="secondary" sx={{ml:'1rem'}}
                               onClick={() => {setMode("edit")}}
                    >
                      수정
                    </VuiButton>
                    <VuiButton color="secondary" sx={{ml:'1rem'}}
                               onClick={() => {
                                 confirmCheck("이 항목을 삭제하면 복구할 수 없습니다. 정말로 삭제하시겠습니까?",() => {postStore.deletePost(postStore.posts[0].uplIdx,history)})
                               }
                               }
                    >
                      삭제
                    </VuiButton>
                  </VuiBox>
                )
              }
              {
                postStore.posts[0] && (
                  <>
                    <VuiInput
                      sx={{mb:'1rem'}}
                      placeholder='제목을 작성하세요'
                      size="large"
                      disabled={true}
                      value={postStore.posts[0].uplTitle}
                    />
                    <MyCkeditor
                      customConfig={{toolbar: []}}
                      contents={postStore.posts[0].uplContent}
                      customDisabled={{editDisabled:true}}
                      onChange={ ( event, editor ) => {
                        postStore.handleContentChange(0,editor.getData());
                      }}
                    />
                  </>
                )
              }
              </>
            )
          }
          {
            mode === "edit" && postStore.posts[0] && (
              <>
                <Box sx={{ minWidth: 150, width: '100%', marginBottom:'1rem',
                  ".MuiSelect-select": {minWidth: "100%"} ,
                }}>
                  <FormControl sx={{width:'50%'}}>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={menuIdx}
                      label="메인메뉴"
                      onChange={handleMenuIdxChange}
                    >
                      {
                        menuStore.menus.map(x => {
                          return <MenuItem value={x.menuIdx} selected={x.menuIdx === menuIdx} key={x.menuIdx}>{x.menuName}</MenuItem>
                        })
                      }
                    </Select>
                  </FormControl>
                  <FormControl sx={{width:'50%'}}>
                    <Select
                      labelId="demo-simple-select-label2"
                      id="demo-simple-select2"
                      value={menuSubIdx}
                      label="서브메뉴"
                      onChange={handleSubMenuIdxChange}
                    >
                      {
                        menuStore.menus.filter(x => x.menuIdx === menuIdx).map(x => {
                          return x.subMenus.map(subMenu => {
                            return <MenuItem value={subMenu.menuIdx} selected={subMenu.menuIdx === menuSubIdx} key={subMenu.menuIdx}>{subMenu.menuName}</MenuItem>
                          })
                        })
                      }
                    </Select>
                  </FormControl>
                </Box>
                <VuiInput
                  sx={{mb:'1rem'}}
                  placeholder='제목을 작성하세요'
                  size="large"
                  value={postStore.posts[0].uplTitle}
                  onChange = {(event) => postStore.handleTitleChange(0,event.target.value)}
                />
                <MyCkeditor
                  customConfig={CkeditorConfig}
                  contents={postStore.posts[0].uplContent}
                  onChange={ ( event, editor ) => {
                    postStore.handleContentChange(0,editor.getData());
                  }}
                />
            <VuiBox display="flex" sx={{mt:'1rem',justifyContent:'center'}} >
              <VuiButton color="secondary" sx={{ml:'1rem'}}
                         onClick={() => {setMode("view")}}
              >
                취소
              </VuiButton>
              <VuiButton color="secondary" sx={{ml:'1rem'}}
                         onClick={() => {
                           confirmCheck("저장하시겠습니까?",() => {
                             postStore.modifyPost(postStore.posts[0].uplTitle,postStore.posts[0].uplContent,postStore.posts[0].uplIdx,menuSubIdx)
                               .then(
                                 res => {
                                 postPagingStore.pageChange(0);
                                   history.goBack();
                                 })
                           })
                         }}
              >
                저장
              </VuiButton>
            </VuiBox>
              </>
            )}
          {
            mode === "write" && (
              <>
                <VuiInput
                  sx={{mb:'1rem'}}
                  placeholder='제목을 작성하세요'
                  size="large"
                  value={title}
                  onChange = {(event) => setTitle(event.currentTarget.value)}
                />
                <MyCkeditor
                  customConfig={CkeditorConfig}
                  contents={content}
                  onChange={(event,editor) => {
                    setContent(editor.getData());
                  }}
                />
                <VuiBox display="flex" sx={{mt:'1rem',justifyContent:'center'}} >
                  <VuiButton color="secondary" sx={{ml:'1rem'}}
                             onClick={() => {history.goBack()}}
                  >
                    취소
                  </VuiButton>
                  <VuiButton color="secondary" sx={{ml:'1rem'}}
                             onClick={() => {
                               confirmCheck("저장하시겠습니까?",
                                 () => {
                                 if(title.length === 0)
                                 {
                                   window.alert("제목을 입력해주세요");
                                   return false;
                                 }
                                 postStore.createPost(title,content,menuSubIdx)
                                   .then(
                                     res => {
                                       postPagingStore.pageChange(0);
                                       history.goBack();
                                     })}
                               )
                             }}
                  >
                    저장
                  </VuiButton>
                </VuiBox>
              </>
            )
          }
        </Card>
        { mode === "view" && (
          <VuiBox sx={{mt:'1rem'}}>
            <Card sx={{mb:'1rem'}}>
              <VuiBox sx={{mt:'1rem',display:'flex'}}>
                <VuiInput placeholder='아이디를 입력하세요' sx={{color:'#a0aec0',}} value={uclNickname}
                          onChange={(event) => {setUclNickname(event.target.value)}}
                />
                <VuiInput type="password" placeholder='비밀번호를 입력하세요' sx={{color:'#a0aec0',}} value={uclPw}
                          onChange={(event) => {setUclPw(event.target.value)}}
                />
              </VuiBox>
              <VuiInput placeholder='내용을 작성하세요' sx={{color:'#a0aec0',}}
                        value={uclContent}
                        component="TextField"
                        multiline
                        minRows={4}
                        onChange={((event) => {setUclContent(event.target.value)})}
              />
              <VuiButton color="secondary"
                         sx={{mt:'4px'}}
                         onClick={() => {
                           confirmCheck("등록하시겠습니까?",() => {
                             if(postStore.posts[0])
                             {
                               postStore.posts[0].createComment(uclContent,uclNickname,uclPw).then(
                                 window.alert("저장되었습니다.")
                               );
                             }
                           })
                         }}
              >
                댓글 등록
              </VuiButton>
            </Card>
              {
                mode === "view" && postStore.posts[0] && postStore.posts[0].comments.length > 0 && (
                  <TimelineList title="댓글 목록" dark={true}>
                    {postStore.posts[0].comments.map((x) => (
                      renderCommnets(x)
                    ))}
                  </TimelineList>
                )
              }
          </VuiBox>
        )}
      </VuiBox>
    </DashboardLayout>
    )
});

export default BlogPost;