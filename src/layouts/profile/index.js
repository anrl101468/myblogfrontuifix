/*!

=========================================================
* BLOG Free React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/vision-ui-free-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com/)
* Licensed under MIT (https://github.com/creativetimofficial/vision-ui-free-react/blob/master LICENSE.md)

* Design and Coded by Simmmple & Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

// @mui material components
// @mui icons
import Card from "@mui/material/Card";
import Grid from "@mui/material/Grid";

// BLOG Dashboard React components
import VuiBox from "components/VuiBox";
import VuiTypography from "components/VuiTypography";
import ProfileInfoCard from "examples/Cards/InfoCards/ProfileInfoCard";

// BLOG Dashboard React example components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
// Overview page components
import Header from "layouts/profile/components/Header";
import { useStores } from "../../state/context/Context";
import { observer } from "mobx-react";
import TabPanel from '@mui/lab/TabPanel';
import { TabContext } from "@mui/lab";
import React, { lazy, useEffect, useRef } from "react";
import { MyMenu } from "../../components/MyMenu/MyMenu";
import { useHistory } from "react-router-dom/cjs/react-router-dom";
import LineChart from "../../examples/Charts/LineCharts/LineChart";
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import IconButton from "@mui/material/IconButton";
import { Meta } from "../../Meta";

const Overview = observer((props) => {
  const {usrStore,chartItemStore} = useStores();
  const [value, setValue] = React.useState(props.location.state && props.location.state.value ? props.location.state.value : '0');
  const falseRef = useRef(false);
  const history = useHistory();
  useEffect(()=> {
    if(!usrStore.usr)
    {
      window.alert("로그인 해주시기 바랍니다.");
      history.push("/authentication/sign-in");
    }
  },[]);

  useEffect(() => {
      chartItemStore.getChartData();
  },[chartItemStore.year,chartItemStore.month]);

  const handleMonthAdd = () =>
  {
    if(chartItemStore.month === 12)
    {
      chartItemStore.setYear(chartItemStore.year +1);
      chartItemStore.setMonth(1);
    }
    else
    {
      chartItemStore.setMonth(chartItemStore.month +1);
    }
  }

  const handleMonthSubtract = () =>
  {
    if(chartItemStore.month === 1)
    {
      chartItemStore.setYear(chartItemStore.year -1);
      chartItemStore.setMonth(12);
    }
    else
    {
      chartItemStore.setMonth(chartItemStore.month -1);
    }
  }
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <DashboardLayout>
      <TabContext value={value}>
      <Header tabValue={value} tabChange={handleChange}/>
      <VuiBox mt={5} mb={3}>
          <Grid
            item
            xs={12}
            xl={3}
            xxl={3}
            sx={({ breakpoints }) => ({
              [breakpoints.only("xl")]: {
                gridArea: "1 / 2 / 2 / 3",
              },
            })}
          >
            <TabPanel value='0'>
              {
                usrStore.usr && usrStore.usr.usrGt && (
                  <ProfileInfoCard
                    title="소개글"
                    description={usrStore.usr.usrGt}
                    info={{
                      fullName: usrStore.usr.usrName,
                    }}
                  />
                )
              }

              <VuiBox sx={{mt:'1rem'}}>
                <Card>
                  <VuiTypography variant="lg" fontWeight="bold" color="white" textTransform="capitalize">
                    방문자수 : {usrStore.usr && (usrStore.usr.uvtCnt)}
                  </VuiTypography>
                </Card>
                <VuiBox sx={{mt:'1rem'}}>
                  <Card>
                    <VuiBox sx={{mt:'1rem'}} sx={{display:'flex',justifyContent:'space-between'}}>
                      <IconButton onClick={handleMonthSubtract}>
                        <ArrowBackIosNewIcon color="white"/>
                      </IconButton>
                      <VuiTypography variant="lg" fontWeight="bold" color="white" textTransform="capitalize" sx={{textAlign:'center'}}>
                        {`${chartItemStore.year}년 ${chartItemStore.month}월(${chartItemStore.totalChartCnt}명)`}
                      </VuiTypography>
                      <IconButton onClick={handleMonthAdd}>
                        <ArrowForwardIosIcon color="white"/>
                      </IconButton>
                    </VuiBox>
                        <LineChart
                          lineChartData={chartItemStore.chartItem}
                          lineChartOptions={chartItemStore.categoryItem}
                        />
                  </Card>
                </VuiBox>
              </VuiBox>
            </TabPanel>
            <TabPanel value='1'>
              <MyMenu />
            </TabPanel>
          </Grid>
      </VuiBox>
      </TabContext>
      {/*<Footer />*/}
    </DashboardLayout>
  );
});

export default Overview;
