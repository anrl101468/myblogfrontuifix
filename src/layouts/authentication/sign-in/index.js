/*!

=========================================================
* BLOG Free React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/vision-ui-free-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com/)
* Licensed under MIT (https://github.com/creativetimofficial/vision-ui-free-react/blob/master LICENSE.md)

* Design and Coded by Simmmple & Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

import { useState } from "react";

// react-router-dom components
import { Link, Redirect } from "react-router-dom";

// BLOG Dashboard React components
import VuiBox from "components/VuiBox";
import VuiTypography from "components/VuiTypography";
import VuiInput from "components/VuiInput";
import VuiButton from "components/VuiButton";
import VuiSwitch from "components/VuiSwitch";
import GradientBorder from "examples/GradientBorder";

// BLOG Dashboard assets
import radialGradient from "assets/theme/functions/radialGradient";
import palette from "assets/theme/base/colors";
import borders from "assets/theme/base/borders";

// Authentication layout components
import CoverLayout from "layouts/authentication/components/CoverLayout";

// Images
import bgSignIn from "assets/images/signInImage.webp";
import { useStores } from "../../../state/context/Context";
import { useHistory } from "react-router-dom/cjs/react-router-dom";
import VuiAlert from "components/VuiAlert";
import Icon from "@mui/material/Icon";
import React from 'react';
function SignIn() {

  const [idAlertState,setIdAlertState] = useState(false);
  const [pwAlertState,setPwAlertState] = useState(false);
  const {loginStore} = useStores();

  const history = useHistory();

  const textCheck = (event) =>
  {
    if(event.target.value.match(/[ㄱ-ㅎㅏ-ㅣ가-힣 \s]/g))
    {
      event.target.value = event.target.value.replace(/[ㄱ-ㅎㅏ-ㅣ가-힣 \s]/g, "");
      window.alert("한글 또는 공백은 입력하실수 없습니다.");
      return false;
    }
    return true;
  }

  const loginGo = () =>
  {
    if(loginStore.usrId.length >= 4)
    {
      if(loginStore.usrPw.length >= 4)
      {
        loginStore.handleLogin(history);
      }
      else
      {
        setPwAlertState(true);
      }
    }
    else
    {
      setIdAlertState(true);
    }
  }
  return (
    <CoverLayout
      title="만나서 반갑습니다.!"
      color="white"
      description="아이디와 비밀번호를 입력하여 로그인할수 있습니다."
      premotto="INSPIRED BY THE FUTURE:"
      motto="YOUR MAKE BLOG"
      image={bgSignIn}
    >
      <VuiBox component="form" role="form">
        <VuiBox mb={2}>
          <VuiBox mb={1} ml={0.5}>
            <VuiTypography component="label" variant="button" color="white" fontWeight="medium">
              아이디
            </VuiTypography>
          </VuiBox>
          <GradientBorder
            minWidth="100%"
            padding="1px"
            borderRadius={borders.borderRadius.lg}
            backgroundImage={radialGradient(
              palette.gradients.borderLight.main,
              palette.gradients.borderLight.state,
              palette.gradients.borderLight.angle
            )}
          >
            <VuiInput type="text"
                      placeholder="아이디를 입력해주세요"
                      fontWeight="500"
                      onChange={
                      (event) =>
                      {
                        if(textCheck(event))
                        {
                          loginStore.handleUsrIdChange(event);
                        }
                      }
                      }
            />
          </GradientBorder>
        </VuiBox>
        <VuiBox mb={2}>
          <VuiBox mb={1} ml={0.5}>
            <VuiTypography component="label" variant="button" color="white" fontWeight="medium">
              비밀번호
            </VuiTypography>
          </VuiBox>
          <GradientBorder
            minWidth="100%"
            borderRadius={borders.borderRadius.lg}
            padding="1px"
            backgroundImage={radialGradient(
              palette.gradients.borderLight.main,
              palette.gradients.borderLight.state,
              palette.gradients.borderLight.angle
            )}
          >
            <VuiInput
              type="password"
              placeholder="비밀번호를 입력해주세요"
              sx={({ typography: { size } }) => ({
                fontSize: size.sm,
              })}
              onChange={ (event) =>
                {
                  if(textCheck(event))
                  {
                    loginStore.handleUsrPwChange(event);
                  }
                }
              }
              onKeyDown={(event) => {
              if(event.keyCode === 13){
                  loginGo();
              }}}
            />
          </GradientBorder>
        </VuiBox>
        <VuiBox mt={4} mb={1}>
          <VuiButton color="info"
                     fullWidth
                     onClick={loginGo}
          >
            로그인
          </VuiButton>
          <VuiBox mt={2}>
            {
              idAlertState && (
                <VuiAlert sx={{fontSize:'0.7em'}} dismissible={true}>아이디를 4글자 이상 입력하세요.</VuiAlert>
              )
            }
            {
              pwAlertState && (
                <VuiAlert sx={{fontSize:'0.6em'}} dismissible={true}>비밀번호를 4글자 이상 입력하세요.</VuiAlert>
              )
            }
          </VuiBox>

        </VuiBox>
        <VuiBox mt={3} textAlign="center">
          <VuiTypography variant="button" color="text" fontWeight="regular">
            회원가입하러 가기 {" "} &nbsp;&nbsp;&nbsp;
            <VuiTypography
              component={Link}
              to="/authentication/sign-up"
              variant="button"
              color="white"
              fontWeight="medium"
            >
              회원가입
            </VuiTypography>
          </VuiTypography>
        </VuiBox>
      </VuiBox>
    </CoverLayout>
  );
}

export default SignIn;
