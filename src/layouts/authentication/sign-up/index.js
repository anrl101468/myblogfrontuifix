/*!

=========================================================
* BLOG Free React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/vision-ui-free-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com/)
* Licensed under MIT (https://github.com/creativetimofficial/vision-ui-free-react/blob/master LICENSE.md)

* Design and Coded by Simmmple & Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

import { useEffect, useState } from "react";

// react-router-dom components
import { Link } from "react-router-dom";

// BLOG Dashboard React components
import VuiBox from "components/VuiBox";
import VuiTypography from "components/VuiTypography";
import VuiInput from "components/VuiInput";
import VuiButton from "components/VuiButton";
import VuiSwitch from "components/VuiSwitch";
import GradientBorder from "examples/GradientBorder";

// BLOG Dashboard assets
import radialGradient from "assets/theme/functions/radialGradient";
import rgba from "assets/theme/functions/rgba";
import palette from "assets/theme/base/colors";
import borders from "assets/theme/base/borders";
import React from 'react';
// Authentication layout components
import CoverLayout from "layouts/authentication/components/CoverLayout";

// Images
import bgSignIn from "assets/images/signUpImage.webp";
import VuiAlert from "../../../components/VuiAlert";
import { JoinStore } from "../../../state/store/usr/JoinStore";
import { useStores } from "../../../state/context/Context";
import { useHistory } from "react-router-dom/cjs/react-router-dom";
import { observer } from "mobx-react";

const SignIn = observer(() => {

  const [alertOpen,setAlertOpen] = useState(false);
  const [alertText,setAlertText] = useState("");
  const [usrIdDcheck,setUsrIdDcheck] = useState(true);
  const {joinStore,usrStore} = useStores();

  const history = useHistory();

  useEffect(() => {
    if(usrStore.usr)
    {
      window.alert("이미 로그인 되었습니다.");
      history.goBack();
    }
  },[]);
  const joinProc = () =>
  {
    if(joinStore.usrName.length === 0)
    {
      setAlertOpen(true);
      setAlertText("이름은 필수 입력 항목입니다.");
      return false;
    }
    if(joinStore.usrId.length < 4)
    {
      setAlertOpen(true);
      setAlertText("아이디를 4글자 이상입력해주세요.");
      return false;
    }
    if(joinStore.usrPw.length < 4)
    {
      setAlertOpen(true);
      setAlertText("비밀번호를 4글자 이상입력해주세요.");
      return false;
    }

    setAlertOpen(false);
    return true;
  }
  const textCheck = (event) =>
  {


    if (event.target.name === 'name')
    {
      if(event.target.value.match(/\s/g))
      {
        event.target.value = event.target.value.replace(/\s/g, "");
        window.alert("공백을 입력하실수 없습니다.");
        return false;
      }
      else
      {
        return true;
      }
    }
    else if (event.target.name === 'id' || event.target.name === 'password')
    {
      if(event.target.value.match(/[ㄱ-ㅎㅏ-ㅣ가-힣 \s]/g))
      {
        event.target.value = event.target.value.replace(/[ㄱ-ㅎㅏ-ㅣ가-힣 \s]/g, "");
        window.alert("한글 또는 공백을 입력하실수 없습니다.");
        return false;
      }
      else
      {
        return true;
      }
    }

  }
  return (
    <CoverLayout
      title="만나서 반갑습니다.!"
      color="white"
      description="아이디와 비밀번호를 입력하여 로그인할수 있습니다."
      premotto="INSPIRED BY THE FUTURE:"
      motto="YOUR MAKE BLOG"
      image={bgSignIn}
    >
      <GradientBorder borderRadius={borders.borderRadius.form} minWidth="100%" maxWidth="100%">
        <VuiBox
          component="form"
          role="form"
          borderRadius="inherit"
          p="45px"
          sx={({ palette: { secondary } }) => ({
            backgroundColor: secondary.focus,
          })}
        >
          <VuiTypography
            color="white"
            fontWeight="bold"
            textAlign="center"
            mb="24px"
            sx={({ typography: { size } }) => ({
              fontSize: size.lg,
            })}
          >
            회원가입
          </VuiTypography>
          {
            alertOpen && (
              <VuiAlert sx={{mt:'1rem',fontSize:'0.7em'}}>{alertText}</VuiAlert>
            )
          }
          <VuiBox mb={2}>
            <VuiBox mb={1} ml={0.5}>
              <VuiTypography component="label" variant="button" color="white" fontWeight="medium">
                이름
              </VuiTypography>
            </VuiBox>
            <GradientBorder
              minWidth="100%"
              borderRadius={borders.borderRadius.lg}
              padding="1px"
              backgroundImage={radialGradient(
                palette.gradients.borderLight.main,
                palette.gradients.borderLight.state,
                palette.gradients.borderLight.angle
              )}
            >
              <VuiInput
                placeholder="이름이나 사용할 닉네임을 입력해주세요"
                sx={({ typography: { size } }) => ({
                  fontSize: size.sm,
                })}
                name="name"
                onChange={ (event) =>
                  {
                    if(textCheck(event))
                    {
                      joinStore.handleUsrNameChange(event);
                    }
                  }
                }
              />
            </GradientBorder>
          </VuiBox>
          <VuiBox mb={2}>
            <VuiBox mb={1} ml={0.5}>
              <VuiTypography component="label" variant="button" color="white" fontWeight="medium">
                아이디
              </VuiTypography>
            </VuiBox>
            <VuiBox>
              <GradientBorder
                minWidth="100%"
                borderRadius={borders.borderRadius.lg}
                padding="1px"
                backgroundImage={radialGradient(
                  palette.gradients.borderLight.main,
                  palette.gradients.borderLight.state,
                  palette.gradients.borderLight.angle
                )}
              >
                <VuiInput
                  type="text"
                  placeholder="아이디를 입력해주세요"
                  sx={({ typography: { size } }) => ({
                    fontSize: size.sm,
                  })}
                  name="id"
                  value={joinStore.usrId}
                  onChange={
                    (event) =>
                    {
                      if(textCheck(event))
                      {
                        joinStore.handleUsrIdChange(event);
                        setUsrIdDcheck(true);
                      }
                    }
                  }
                />
              </GradientBorder>
              <VuiButton color="primary" size="small" sx={{mt:'5px'}}
                         onClick=
                         {()=>
                           {
                             if(joinStore.usrId.length === 0 || joinStore.usrId.length < 4) {
                               window.alert("네글자이상 입력해주시기 바랍니다.");
                               return false;
                             }
                             if(usrIdDcheck)
                             {
                              joinStore.handleDuplicateCheck(joinStore.usrId);
                              setUsrIdDcheck(false);
                             }
                             else
                             {
                               if(joinStore.dCheck)
                               {
                                 window.alert("이미 중복검사를 하셧습니다.");
                               }
                               else{
                               window.alert("아이디를 변경해주시기 바랍니다.");
                               }
                             }
                           }
                         }
              >
                중복검사
              </VuiButton>
            </VuiBox>
          </VuiBox>
          <VuiBox mb={2}>
            <VuiBox mb={1} ml={0.5}>
              <VuiTypography component="label" variant="button" color="white" fontWeight="medium">
                비밀번호
              </VuiTypography>
            </VuiBox>
            <GradientBorder
              minWidth="100%"
              borderRadius={borders.borderRadius.lg}
              padding="1px"
              backgroundImage={radialGradient(
                palette.gradients.borderLight.main,
                palette.gradients.borderLight.state,
                palette.gradients.borderLight.angle
              )}
            >
              <VuiInput
                type="password"
                name="password"
                placeholder="비밀번호를 입력해주세요."
                sx={({ typography: { size } }) => ({
                  fontSize: size.sm,
                })}
                onChange={
                  (event) =>
                  {
                    if(textCheck(event))
                    {
                      joinStore.handleUsrPwChange(event);
                    }
                  }
                }
              />
            </GradientBorder>
          </VuiBox>
          <VuiBox mt={4} mb={1}>
            <VuiButton color="info" fullWidth
                       onClick={ () => {
                         if(joinProc())
                         {
                          joinStore.joinAxios(history);
                         }
                       }
                      }
            >
              가입하기
            </VuiButton>
          </VuiBox>
          <VuiBox mt={3} textAlign="center">
            <VuiTypography variant="button" color="text" fontWeight="regular">
              계정이 있으신가요?{" "}
              <VuiTypography
                component={Link}
                to="/authentication/sign-in"
                variant="button"
                color="white"
                fontWeight="medium"
              >
                로그인
              </VuiTypography>
            </VuiTypography>
          </VuiBox>
        </VuiBox>
      </GradientBorder>
    </CoverLayout>
  );
});

export default SignIn;
