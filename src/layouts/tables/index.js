/*!

=========================================================
* BLOG Free React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/vision-ui-free-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com/)
* Licensed under MIT (https://github.com/creativetimofficial/vision-ui-free-react/blob/master LICENSE.md)

* Design and Coded by Simmmple & Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

// @mui material components
import Card from "@mui/material/Card";

// BLOG Dashboard React components
import VuiBox from "components/VuiBox";
import VuiTypography from "components/VuiTypography";

// BLOG Dashboard React example components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Table from "examples/Tables/Table";

// Data
import { Link, NavLink, Redirect, useLocation, useParams } from "react-router-dom";
import { useStores } from "../../state/context/Context";
import { observer } from "mobx-react";
import { useEffect, useRef, useState } from "react";
import React from "react";
import { TablePagination } from "@mui/material";
import VuiButton from "../../components/VuiButton";
import { v4 as uuidv4 } from "uuid";
import { useHistory } from "react-router-dom/cjs/react-router-dom";
import VuiInput from "../../components/VuiInput";
import { Meta } from "../../Meta";

const Tables = observer(() => {

  const {postStore,usrStore,postPagingStore,menuStore} = useStores();
  const history = useHistory();
  const params = useParams();
  const mounted = useRef(false);
  const mounted2 = useRef(false);

  let tmepMenuIdx = -1;
    menuStore.menus.some((menu, menuIndex) => { // some() 메소드는 조건을 만족하는 요소를 찾으면 동작을 멈춥니다.
    const subMenuIndex = menu.subMenus.findIndex(subMenu => {
      return subMenu.menuName === params.menuName;
    });
    if (subMenuIndex >= 0) {
      tmepMenuIdx = menuStore.menus[menuIndex].subMenus[subMenuIndex].menuIdx
      return true; // some() 메소드는 조건을 만족하는 요소를 찾으면 true를 반환하여 동작을 멈춥니다.
    }
  });

  if(tmepMenuIdx !== -1)
  {
    postStore.setMenuIdx(tmepMenuIdx);
  }
  useEffect(()=> {
    if(!params.usrId)
    {
      history.push("/base")
    }
    if(params.usrId && menuStore.menus.length === 0 || params.usrId && usrStore.usrId !== params.usrId || usrStore.usr)
    {
      menuStore.getMainMenuServerInfo(params.usrId)
        .then((res) =>{
          if(menuStore.menus.length === 0)
          {
            window.alert("등록되지 않은 ID이거나 등록된 메뉴가 없습니다.");
            history.goBack();
          }
          else
          {
            usrStore.setUsrId(params.usrId)
          }
        })
    }

  },[]);

  useEffect(()=> {
    if(mounted.current === true)
    {
      getPost();
    }
    mounted.current = true;
  },[postPagingStore.page,postPagingStore.rowsPerPage]);

  useEffect(() => {
    if(postStore.menuIdx > 0) {
      getCnt();
      getPost();
    }
  },[postStore.menuIdx])
  const getCnt = () =>
  {
    const data = {
      menuIdx : postStore.menuIdx,
      page : postPagingStore.page,
      rowsPerPage : postPagingStore.rowsPerPage,
      searchKeyword : postStore.searchKeyword,
      usrId : params.usrId,
    }
    postStore.selectPostCnt(data);
  }

  const getPost = () =>
  {
    const data = {
      menuIdx : postStore.menuIdx,
      page : postPagingStore.page,
      rowsPerPage : postPagingStore.rowsPerPage,
      searchKeyword : postStore.searchKeyword,
      usrId : params.usrId,
    }
    postStore.selectPost(data);
  }

  const handleSeachClick = () =>
  {
    postPagingStore.pageChange(0);
    getCnt();
    getPost();
  }
  const pageChange = (event,newPage) =>
  {
    postPagingStore.pageChange(newPage);
  }

  const rowsChange = (event) =>
  {
    postPagingStore.rowsPerPageChange(parseInt(event.target.value));
  }
  let rows = [];

  const columns= [
    { name: "제목", align: "center" , width: "70%"},
    { name: "작성일", align: "center" , width: "15%"},
    { name: "조회수", align: "center" , width: "15%"},
  ];

  if (postStore.posts.length > 0) {
    rows = postStore.posts
      rows = rows.map((x,i) => ({
        제목: (
          <NavLink key={uuidv4()} to={`/user/${params.usrId}/${params.menuName}/${x.uplIdx}`}>
            <VuiTypography variant="caption" fontWeight="medium" color="white">
              {x.uplTitle}
            </VuiTypography>
          </NavLink>
        ),
        작성일: (
          <VuiTypography variant="caption" color="white" fontWeight="medium" key={uuidv4()}>
            {x.uplRegDt.substring(0,16)}
          </VuiTypography>
        ),
        조회수: (
          <VuiTypography variant="caption" color="white" fontWeight="medium" key={uuidv4()}>
            {x.uplVcnt}
          </VuiTypography>
        ),
      }));
  }else {
    rows = [
      {
        제목: (
          <VuiTypography variant="caption" fontWeight="medium" color="white" key={uuidv4()}>
            등록된 글이 없습니다.
          </VuiTypography>
        ),
        작성일: (
          <></>
        ),
        조회수: (
          <></>
        ),
      }
    ]
  }


  return (
    <DashboardLayout>
      <Meta
        type="web"
        url={window.location.href}
        title={params.menuName}
        description={`${params.id}님의 블로그입니다.`}
        siteNm="myBlog"
      />
      <DashboardNavbar />
      <VuiBox py={3}>
        <VuiBox mb={3}>
          <Card>
            <VuiBox
              sx={{
                "& th": {
                  borderBottom: ({ borders: { borderWidth }, palette: { grey } }) =>
                    `${borderWidth[1]} solid ${grey[700]}`,
                },
                "& .MuiTableRow-root:not(:last-child)": {
                  "& td": {
                    borderBottom: ({ borders: { borderWidth }, palette: { grey } }) =>
                      `${borderWidth[1]} solid ${grey[700]}`,
                  }
                }
            }}
            >
              <Table columns={columns} rows={rows} />
              <TablePagination
                rowsPerPageOptions={[5, 10, 25, 50]}
                component="div"
                count={postStore.totalCnt}
                rowsPerPage={postPagingStore.rowsPerPage}
                page={postPagingStore.page}
                onPageChange={pageChange}
                onRowsPerPageChange={rowsChange}
                showLastButton={true}
                showFirstButton={true}
                labelRowsPerPage="페이지당 글 목록수"
                labelDisplayedRows={({ from, to, count }) => `${postPagingStore.page + 1}페이지 (${from}-${to} / ${count})`}
                sx={{
                  " .MuiInputBase-root": {
                    maxWidth:'fit-content'
                  },
                  mt:'1rem',
                  " .MuiToolbar-root" : {
                    color:'#a0aec0'
                  },
                  " .MuiIconButton-sizeMedium": {
                    color: "gainsboro"
                  }
                }}
              />
            </VuiBox>
          </Card>
        </VuiBox>
          <VuiBox display="flex" justifyContent="space-between" alignItems="center" mb="22px">
            <VuiBox>
            {
              usrStore.usr && usrStore.usr.usrId === params.usrId && (
                  <VuiButton color="secondary">
                    <Link to="/post" style={{color:'#a0aec0'}}>
                      글 작성
                    </Link>
                  </VuiButton>
              )
            }
            </VuiBox>
              <VuiBox display='flex'>
                <VuiInput
                  mr="10px"
                  placeholder="검색어를 입력하세요."
                  value={postStore.searchKeyword}
                  onChange={(event) => {postStore.setSearchKeword(event.target.value)}}
                  icon={{ component: "search",
                    direction: "left",
                  }}
                  sx={({ breakpoints }) => ({
                    [breakpoints.down("sm")]: {
                      maxWidth: "80px",
                    },
                    [breakpoints.only("sm")]: {
                      maxWidth: "80px",
                    },
                    backgroundColor: "info.main !important",
                  })}
                />
                <VuiButton color="secondary" sx={{ml:'1rem'}} onClick={handleSeachClick}>
                  검색
                </VuiButton>
              </VuiBox>

          </VuiBox>

      </VuiBox>
      {/*<Footer />*/}
    </DashboardLayout>
  );
});

export default Tables;
