/*!

=========================================================
* BLOG Free React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/vision-ui-free-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com/)
* Licensed under MIT (https://github.com/creativetimofficial/vision-ui-free-react/blob/master LICENSE.md)

* Design and Coded by Simmmple & Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the software.

*/

import React from "react";
import { createRoot} from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import App from "App";
// BLOG Dashboard React Context.jsx Provider
import { VisionUIControllerProvider } from "context";
import { StoreProvider } from "./state/context/Context";
import { RootStore } from "./state/store/RootStore";
import { HelmetProvider } from 'react-helmet-async';

const rootElement = document.getElementById('root');
const root = createRoot(rootElement);
const rootStore = new RootStore();
const process = require('process/browser');

root.render(
  <BrowserRouter>
  <VisionUIControllerProvider>
    <StoreProvider value={rootStore}>
      <HelmetProvider>
        <App />
      </HelmetProvider>
    </StoreProvider>
  </VisionUIControllerProvider>
</BrowserRouter>
)

