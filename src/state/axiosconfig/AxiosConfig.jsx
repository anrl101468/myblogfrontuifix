import axios from "axios";

export const instance = axios.create();

instance.interceptors.request.use(
  async function(config) {

    config.withCredentials = true;
    const authToken = localStorage.getItem("Authorization");
    const tokenIdx = localStorage.getItem("tokenIdx");
    const ip = localStorage.getItem("client-ip");
    if (authToken) {
      config.headers.Authorization = authToken;
    }
    if(tokenIdx)
    {
      config.headers.tokenIdx = tokenIdx;
    }
    if (ip) {
      config.headers.ip = ip;
    }
    if (!ip) {
      await axios.get('https://geolocation-db.com/json/')
        .then(res => {
          const clientIp = res.data.IPv4;
          if (clientIp) {
            localStorage.setItem("client-ip", clientIp);
            config.headers.ip = clientIp;
          }
        });
    }
    /*console.log("config = " + JSON.stringify(config));*/
    return config;
  },
  function (error) {
    // 오류 요청을 보내기전 수행할 일
    // ...
    return Promise.reject(error);
  });