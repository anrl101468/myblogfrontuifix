import { action, makeObservable, observable } from "mobx";

export class MenuModalStore
{
  rootStore;

  addOpen= false;
  modifyOpen= false;
  menuTitle = "";
  menuIdx='';
  constructor(rootStore) {
    this.rootStore = rootStore;
    this.addOpen=false;
    this.menuTitle="";
    this.menuIdx="";
    makeObservable(this,{
      addOpen:observable,
      modifyOpen:observable,
      menuTitle:observable,
      menuIdx:observable,
      setAddOpen:action,
      setModifyOpen:action,
      setMenuTitle:action,
    })
  }

  setAddOpen(value)
  {
    this.addOpen = value;
  }

  setModifyOpen(value)
  {
    this.modifyOpen = value;
  }

  setMenuTitle(value)
  {
    this.menuTitle = value;
  }
  setMenuIdx(value)
  {
    this.menuIdx = value;
  }
}