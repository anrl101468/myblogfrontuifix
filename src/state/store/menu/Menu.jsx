import { action, makeObservable, observable, runInAction } from "mobx";
import { SubMenu } from "./SubMenu";
import qs from "qs";
import { useStores } from "../../context/Context";

export class Menu
{
  instance;
  menuName;
  buttonId;
  menuId;
  open;
  menuUpIdx;
  anchorEl;
  menuIdx;
  subMenus;

  constructor(instance,menuName, buttonId, menuId, open, menuUpIdx, menuIdx, usrId) {
    this.menuName = menuName;
    this.buttonId = buttonId;
    this.menuId = menuId;
    this.open = open;
    this.menuUpIdx = menuUpIdx;
    this.menuIdx = menuIdx;
    this.anchorEl = null;
    this.subMenus = [];

    this.instance = instance;
    this.getSubMenuServerInfo(usrId);
    makeObservable(this,{
      menuName:observable,
      buttonId:observable,
      menuId:observable,
      open:observable,
      menuUpIdx:observable,
      menuIdx:observable,
      anchorEl:observable,
      subMenus:observable,
      getSubMenuServerInfo:action,
    })
  }

  getSubMenuServerInfo = async (usrId) => {

    const url = "/api/menu/menuSelect";
    const data = {
      menuUpIdx: this.menuIdx,
      usrId : usrId
    }
    const qsData = qs.stringify(data);
    const res = await this.instance.post(url, qsData);
    runInAction(() => {
      const subMenus = [];
      res.data.forEach((data) => {
        subMenus.push(new SubMenu(data.menuUpIdx, data.menuIdx, data.menuName));
      });
      this.subMenus = subMenus;
    });
  }
}
