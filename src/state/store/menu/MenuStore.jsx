import { action, makeObservable, observable, runInAction } from "mobx";
import { Menu } from "./Menu";
import qs from "qs";
import { v4 as uuidv4 } from "uuid";

export class MenuStore
{
  rootStore;

  menus;

  recentCreateMenuIdx;
  constructor(root) {
    this.rootStore = root;
    this.menus = [];
    this.recentCreateMenuIdx = '';

    makeObservable(this,{
      menus:observable,
      recentCreateMenuIdx:observable,
      setRecentCreateMenuIdx:action,
      getMainMenuServerInfo:action,
      blogVisitorAdd:action,
      createLeftMenu:action,
      modifyLeftMenu:action,
      deleteMenu:action,
      setMenus:action,
    })
  }
  setRecentCreateMenuIdx = (value) =>
  {
    this.recentCreateMenuIdx = value;
  }
  getMainMenuServerInfo = async (usrId) => {
    const url = "/api/menu/menuSelect";
    const data = {
      usrId : usrId,
    }
    const qsData = qs.stringify(data);

    await this.rootStore.instance.post(url,qsData)
      .then(res => {
        runInAction(() => {
          if(res.data.length > 0)
          {
            this.menus = res.data.map(data => {
              return new Menu(this.rootStore.instance,data.menuName, uuidv4() + data.menuIdx, "menu" + data.menuIdx, data.menuIdx === this.recentCreateMenuIdx, data.menuUpIdx, data.menuIdx,  usrId)
            });
          }
        });
        if(res.data.length > 0 && usrId && !this.rootStore.usrStore.usr)
        {
          this.blogVisitorAdd(qsData);
        }
        return res;
      }).catch(error => {
        console.error(error);
      })
  }

  blogVisitorAdd = async (data) => {
    const url = "/api/uvt/uvtAdd";
    await this.rootStore.instance.post(url,data);
  }
  setMenus = (value) =>
  {
    this.menus = value;
  }
  createLeftMenu = async (menuName,menuUpIdx) => {
    const url = "/api/menu/menuCreate";

    const menuSize = this.menus.length;
    let subMenuSize;
    if(!menuUpIdx && menuSize === 8)
    {
      window.alert("메뉴는 8개까지 등록가능합니다");
      return false;
    }
    else if(menuUpIdx)
    {
      const menuIndex = this.menus.findIndex((x) => x.menuIdx === menuUpIdx);
      subMenuSize = this.menus[menuIndex].subMenus.length;

      if(subMenuSize === 8)
      {
        window.alert("서브메뉴는 8개까지 등록가능합니다");
        return false;
      }
    }

    let data = {
      menuName: menuName,
    }
    if(menuUpIdx)
    {
      data = {
        menuName: menuName,
        menuUpIdx : menuUpIdx,
      }
    }

    const qsData = qs.stringify(data);

    await this.rootStore.instance.post(url, qsData)
      .then(res => {
        if (res.status === 200) {
          if(menuUpIdx)
          {
            this.setRecentCreateMenuIdx(menuUpIdx);
          }else if(!menuUpIdx)
          {
            this.setRecentCreateMenuIdx(res.data.menuIdx);
          }
          this.getMainMenuServerInfo();
        }
      }).catch(error => {
        console.log(error);
      })
  }

  modifyLeftMenu = async (menuName,menuIdx) => {
    const url = "/api/menu/menuUpdate"
    const data = {
      menuName : menuName,
      menuIdx : menuIdx,
    }
    const qsData = qs.stringify(data);

    await this.rootStore.instance.post(url, qsData)
      .then(res => {
        if (res.data) {
          this.getMainMenuServerInfo();
          this.rootStore.menuModalStore.setMenuTitle('');
          this.rootStore.menuModalStore.setMenuIdx(null);
        }
        else
        {
          console.log("메뉴이름 업데이트에 실패하였습니다.");
        }
      }).catch(error => {
        console.log(error);
      })
  }

  deleteMenu = async (menuIdx) => {
    const url = "/api/menu/menuDelete"
    const data = {
      menuIdx: menuIdx,
      menuUpIdx: menuIdx
    }

    const qsData = qs.stringify(data);
    await this.rootStore.instance.post(url, qsData)
      .then( res => {
        console.log(res);
      }).catch(error => {
        console.error(error);
      })
    await this.getMainMenuServerInfo();
  }
}