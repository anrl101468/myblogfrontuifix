import {makeObservable, observable} from "mobx";

export class SubMenu
{
    menuName = '';
    menuIdx = '';
    menuUpIdx = '';
    constructor(menuUpIdx,menuIdx,menuName) {
        this.menuUpIdx = menuUpIdx;
        this.menuIdx = menuIdx;
        this.menuName = menuName;
        makeObservable(this,{
            menuName:observable,
            menuIdx:observable,
            menuUpIdx:observable,
        })
    }

}