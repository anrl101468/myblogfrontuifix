import { action, makeObservable, observable } from "mobx";
import qs from "qs";

export class JoinStore
{
  rootStore;

  usrId='';
  usrPw='';
  usrName='';

  dCheck =false;
  constructor(rootStore) {
    this.rootStore = rootStore;

    this.dCheck =false;

    makeObservable(this,{
      usrId:observable,
      usrPw:observable,
      usrName:observable,
      dCheck:observable,
      setDCheck:action,
      setUsrId:action,
      handleUsrIdChange:action,
      handleUsrPwChange:action,
      handleUsrNameChange:action,
      joinAxios:action,
      handleDuplicateCheck:action,
    })
  }

  setDCheck(value)
  {
    this.dCheck = value;
  }

  setUsrId(value)
  {
    this.usrId = value;
  }
  handleUsrIdChange(event)
  {
    this.usrId = event.target.value;
    this.setDCheck(false);
  }

  handleUsrPwChange(event)
  {
    this.usrPw = event.target.value;
  }

  handleUsrNameChange(event)
  {
    this.usrName = event.target.value;
  }

  joinAxios = async (history) => {
    if (this.dCheck === false) {
      window.alert("중복 체크 해주시기 바랍니다.");
      return false;
    }
    const joinUrl = "/api/joinProc"
    const usr = {
      usrId: this.usrId,
      usrPassword: this.usrPw,
      usrName: this.usrName,
    }
    const qsUsr = qs.stringify(usr);

    await this.rootStore.instance.post(joinUrl, qsUsr
    ).then(res => {
      this.rootStore.authorizationStore.setAuthorization(res.headers.get("Authorization"));
      this.rootStore.usrStore.getServerUserInfo();
      this.rootStore.usrStore.setUsrId(this.usrId);
      localStorage.setItem("tokenIdx",res.headers.get("tokenIdx"));
        history.push('/profile',{ value : "1"})

    })
      .catch((error) => {
        throw new Error(error);
      })
  }

  handleDuplicateCheck(usrId)
  {
    const url = "/api/usr/duplicateCheck";
    const data =
      qs.stringify({
        usrId : usrId,
      })

    this.rootStore.instance.post(url, data).then(res =>{
      if(res.data === false)
      {
        window.alert("이미 존재하는 아이디 입니다.");
        this.setDCheck(false);
        this.setUsrId("");
      }
      else
      {
        window.alert("사용가능한 아이디입니다.");
        this.setDCheck(true)
      }
    });
  }
}