import { action, makeObservable, observable } from "mobx";
import qs from "qs";

export class LoginStore
{
  rootStore;

  usrId;
  usrPw;
  constructor(root) {
    this.rootStore = root;
    this.usrId ='';
    this.usrPw='';
    makeObservable(this,{
      usrId:observable,
      usrPw:observable,
      handleUsrIdChange:action,
      handleUsrPwChange:action
    })
  }

  handleUsrIdChange(event)
  {
    this.usrId = event.target.value;
  }

  handleUsrPwChange(event)
  {
    this.usrPw = event.target.value;
  }

  handleLogin(history)
  {
    const url = "/api/login"
    const usr =
      qs.stringify({
        usrId : this.usrId,
        usrPassword : this.usrPw
      });

    this.rootStore.instance.post(url,usr)
      .then(res => {
        if(res.headers.get("Authorization"))
        {
          localStorage.setItem("usrId",this.usrId);
          localStorage.setItem("tokenIdx",res.headers.get("tokenIdx"));
          this.rootStore.authorizationStore.setAuthorization(res.headers.get("Authorization"));
          window.alert("로그인의 성공하셧습니다.");
          /*this.rootStore.mainLeftRootMenuStore.getMainMenuServerInfo();*/
          this.rootStore.usrStore.getServerUserInfo();
          history.push(`/user/${this.usrId}`);
        }
        else
        {
          window.alert(decodeURIComponent(res.headers.get("loginFail")).replace(/\+/g, ' '));
        }
      })
      .catch( error => {
        throw new Error(error);
      })
  }

}