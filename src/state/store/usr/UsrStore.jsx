import {action, makeObservable, observable} from "mobx";
import qs from "qs";

export class UsrStore
{
    rootStore;

    usr;

    usrId;
    constructor(root) {
        this.rootStore = root;
        this.usr = null;
        this.usrId = null;
        const token = localStorage.getItem("Authorization");
        if(token && this.usr === null){
            this.getServerUserInfo().then((res) =>{
                if(this.usr && (window.location.href.search("/base") !== -1 || window.location.href.search("/profile") !== -1))
                {
                    this.usrId = this.usr.usrId;
                    this.rootStore.menuStore.getMainMenuServerInfo(this.usr.usrId);
                }
            })
        }

        makeObservable(this,{
            usr:observable,
            usrId:observable,
            setUsr:action,
            getServerUserInfo:action,
            setUsrId:action,
            updateUsr:action,
        })
    }

    setUsr(usr)
    {
        this.usr = usr;
    }

    setUsrId(usrId)
    {
        this.usrId = usrId;
    }

    async getServerUserInfo() {
        if(localStorage.getItem("Authorization"))
        {
            const url = "/api/usr/getUsrInfo";
            const data = {
                Authorization : localStorage.getItem("Authorization")
            };
            const qsdata = qs.stringify(data);
            await this.rootStore.instance.post(url, qsdata)
              .then(res => {
                  if(res.headers.get("Authorization"))
                  {
                      localStorage.setItem("usrId",this.usrId);
                      this.rootStore.authorizationStore.setAuthorization(res.headers.get("Authorization"));
                  }
                  if(res.data)
                  {
                      this.setUsr(res.data);
                      this.setUsrId(this.usr.usrId);
                  }
              })
              .catch(error => {
                  console.error(error);
              })
        }
        else{
            this.rootStore.authorizationStore.setAuthorization(null);
        }
    }

    updateUsr(targetCol,value)
    {
        const url = "/api/usr/modifyUsr";
        const data = {
            [targetCol] : value
        }

        const qsData = qs.stringify(data);

        this.rootStore.instance.post(url,qsData)
          .then( res =>
            {
                if(res.data === 0)
                {
                    window.alert("등록에 실패하였습니다.");
                }
                else
                {
                    window.alert("저장되었습니다.");
                    this.getServerUserInfo();
                }
              
            }
        )
    }
}