import { action, computed, makeObservable, observable } from "mobx";
import { observer } from "mobx-react";
import qs from "qs";
import { Post } from "./Post";

export class PostStore {
  rootStore;

  searchKeyword;

  posts = [];

  totalCnt;

  menuIdx;

  constructor(rootStore) {
    this.rootStore = rootStore;
    this.posts = [];
    this.totalCnt = 0;
    this.menuIdx = "";
    this.searchKeyword = "";
    makeObservable(this, {
      // 감시대상
      posts: observable,
      searchKeyword: observable,
      totalCnt: observable,
      menuIdx: observable,

      setTotalCnt: action,
      setMenuIdx: action,
      setSearchKeword:action,

      newPost: action,
      handleTitleChange: action,
      handleContentChange: action,
      selectPost: action,
      selectPostCnt: action,
      deletePost: action,
      createPost: action,
      modifyPost: action,
    })
  }

  setMenuIdx(value) {
    this.menuIdx = value
  }

  setTotalCnt(value) {
    this.totalCnt = value;
  }

  setSearchKeword(value)
  {
    this.searchKeyword = value
  }
  handleTitleChange(index,value)
  {
    this.posts[index].setTitle(value);
  }

  handleContentChange(index,value)
  {
    this.posts[index].setContent(value);
  }

  newPost(res)
  {
    this.posts = [];
    res.data.map( (x) => {
      this.posts.push(
        new Post(this.rootStore.instance ,x.uplIdx,x.uplTitle,x.menuIdx, x.uplContent,x.uplRegDt, x.uplVcnt)
      )
    })

  }

  async selectPost(p) {
    const url = "/api/upl/uplSelect";


    if(p)
    {
    const qsData = qs.stringify(p);
    await this.rootStore.instance.post(url, qsData)
      .then(res => {
        this.newPost(res);
      });
    }
  }

  async selectPostCnt(p) {
    const url = "/api/upl/uplSelectCnt";


    if(p)
    {
      const qsData = qs.stringify(p);
      await this.rootStore.instance.post(url, qsData)
        .then(res => {
          this.setTotalCnt(res.data)
        });
    }
  }

  async deletePost(uplIdx,history) {
    const url = "/api/upl/uplDelete";
    const data = {
      uplIdx: uplIdx,
    }

    const qsData = qs.stringify(data);
    await this.rootStore.instance.post(url, qsData).then(res => {
      history.goBack();
    });
  }

  async createPost(title, content, menuIdx) {
    const url = "/api/upl/uplCreate";
    const data = {
      uplTitle: title,
      uplContent: content,
      menuIdx: this.menuIdx,
    }

    const qsData = qs.stringify(data);
    await this.rootStore.instance.post(url, qsData)
      .then( res => {
      })
      .catch(error => console.log(error));

  }

  async modifyPost(title, content, uplIdx, menuIdx ) {
    const url = "/api/upl/uplUpdate";
    const data = {
      uplTitle: title,
      uplContent: content,
      uplIdx: uplIdx,
      menuIdx : menuIdx
    }

    const qsData = qs.stringify(data);
    await this.rootStore.instance.post(url, qsData).catch(error => console.log(error));
  }

  // comment


}