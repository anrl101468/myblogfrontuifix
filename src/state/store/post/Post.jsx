import { action, makeObservable, observable, runInAction } from "mobx";
import qs from "qs";
import { Comment } from "../comment/Comment";
import { useStores } from "../../context/Context";
export class Post
{
  instance;
  uplIdx;
  uplTitle;
  menuIdx;
  uplContent;
  uplVcnt;
  uplRegDt;
  comments;
  constructor(instance,uplIdx, uplTitle, menuIdx, uplContent, uplRegDt,uplVcnt) {
    this.uplIdx = uplIdx;
    this.uplTitle = uplTitle;
    this.menuIdx = menuIdx;
    this.uplContent = uplContent;
    this.uplRegDt = uplRegDt;
    this.uplVcnt = uplVcnt;
    this.comments = [];
    this.instance = instance;

    makeObservable(this,{
      uplIdx:observable,
      uplTitle:observable,
      menuIdx:observable,
      uplContent:observable,
      uplVcnt:observable,
      uplRegDt:observable,
      comments:observable,

      setTitle:action,
      setContent:action,
      updateVcnt:action,

      //comment
      newComment:action,
      selectComment:action,
      createComment:action,
      modifyComment:action,
      deleteComment:action,
    })
  }

  setTitle(value)
  {
    this.uplTitle = value;
  }

  setContent(value)
  {
    this.uplContent = value;
  }

  async updateVcnt()
  {
    const url = "/api/pv/viewAdd";
    const data = {
      uplIdx : this.uplIdx,
    }

    const qsData = qs.stringify(data);
    await this.instance.post(url,qsData).catch(error => console.log(error));
  }

  newComment(res)
  {
    this.comments = [];
    res.data.map( (x) => {
      this.comments.push(
        new Comment(x.uclIdx, x.uclContent, x.uclRegDt, x.uclNickname, x.uclPw)
      )
    })
  }

  // comment 관련
  async selectComment() {
    const url = "/api/ucl/uclSelect";

    var data =
    {
      uplIdx: this.uplIdx,
    }

    const qsData = qs.stringify(data);
    const res = await this.instance.post(url, qsData);
    runInAction( () => {
          this.newComment(res);
        })
    }

  async createComment(uclContent,uclNickname,uclPw) {
    const url = "/api/ucl/uclCreate";
    const data = {
      uclContent: uclContent,
      uplIdx: this.uplIdx,
      uclNickname: uclNickname,
      uclPw : uclPw
    }

    const qsData = qs.stringify(data);
    await this.instance.post(url, qsData)
      .then(
        () => this.selectComment()
      )
      .catch(error => console.log(error));

  }

  async modifyComment(uclIdx,uclContent) {
    const url = "/api/ucl/uclUpdate";
    const data = {
      uclIdx : uclIdx,
      uclContent : uclContent,
    }

    const qsData = qs.stringify(data);
    await this.instance.post(url, qsData).catch(error => console.log(error));
  }

  async deleteComment(uclIdx,history) {
    const url = "/api/ucl/uclDelete";
    const data = {
      uclIdx: uclIdx,
    }

    const qsData = qs.stringify(data);
    await this.instance.post(url, qsData).then(res => {
      this.selectComment();
      history.goBack();
    });
  }
}