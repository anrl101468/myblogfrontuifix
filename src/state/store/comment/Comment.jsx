import { makeObservable, observable } from "mobx";

export class Comment
{
  uclIdx;
  uclContent;
  uclRegDt;
  uclNickname;
  uclPw;


  constructor(uclIdx, uclContent, uclRegDt, uclNickname, uclPw) {
    this.uclIdx = uclIdx;
    this.uclContent = uclContent;
    this.uclRegDt = uclRegDt;
    this.uclNickname = uclNickname;
    this.uclPw = uclPw;

    makeObservable(this,{
      uclIdx:observable,
      uclContent:observable,
      uclRegDt:observable,
      uclNickname:observable,
      uclPw:observable,
    })
  }
}