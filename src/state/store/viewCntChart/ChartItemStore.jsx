import { action, computed, makeObservable, observable } from "mobx";
import qs from "qs";

export class ChartItemStore
{
  rootStore;
  year;
  month;

  chartItem;
  categoryItem;

  constructor(root) {
    this.rootStore = root;

    this.year= new Date().getFullYear();
    this.month= new Date().getMonth()+1;
    this.chartItem=[];
    this.categoryItem=[];

    makeObservable(this,{
      year:observable,
      month:observable,
      chartItem:observable,
      categoryItem:observable,
      setYear:action,
      setMonth:action,
      setChartItem:action,
      setCategoryItem:action,
      getChartData:action,
      totalChartCnt:computed,
    });
  }

  setYear(value)
  {
    this.year = value;
  }

  setMonth(value)
  {
    this.month = value;
  }
  setChartItem(data)
  {
    this.chartItem = data;
  }

  setCategoryItem(data)
  {
    this.categoryItem = data;
  }
  async getChartData() {
    const data = {
      "year": this.year,
      "month": this.month
    }

    const qsData = qs.stringify(data);
    await this.rootStore.instance.post("/api/uvt/uvtGetViews", qsData)
      .then(res => {
        this.setChartItem([{ name: `방문자수:`, data: res.data.chartItem, }]);
        this.setCategoryItem(res.data.categoryItem);
      })
  }

  get totalChartCnt()
  {
    let cnt = 0;
    if(this.chartItem[0])
    {
      this.chartItem[0].data.map(x => {
        cnt = cnt + x;
      });
    }

    return cnt;
  }

}