import {action, makeObservable, observable} from "mobx";

export class AuthorizationStore
{
    rootStore;

    authorization = null;
    constructor(root) {
        this.rootStore = root;
        if(localStorage.getItem("Authorization") != null)
        {
            this.authorization = localStorage.getItem("Authorization");
        }

        makeObservable(this,{
            authorization:observable,
            setAuthorization:action,
            getAuthorization:action,
        })
    }

    setAuthorization(authorization)
    {
        this.authorization = authorization;
        if(authorization != null)
        {
            localStorage.setItem("Authorization",authorization);
        }
        else {
            this.rootStore.usrStore.setUsr(null);
            localStorage.removeItem("Authorization");
        }
    }

    getAuthorization()
    {
        return this.authorization;
    }
}