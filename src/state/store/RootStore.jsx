import { MenuStore } from "./menu/MenuStore";
import { LoginStore } from "./usr/LoginStore";
import { AuthorizationStore } from "./AuthorizationStore";
import { UsrStore } from "./usr/UsrStore";
import { JoinStore } from "./usr/JoinStore";
import { MenuModalStore } from "./menu/MenuModalStore";
import { PostStore } from "./post/PostStore";
import PostPagingStore from "./post/PostPagingStore";
import { ChartItemStore } from "./viewCntChart/ChartItemStore";
import axios from "axios";

const instanceCreate = () => {
  const instanceTemp = axios.create();
  instanceTemp.interceptors.request.use(
    async function(config) {

      config.withCredentials = true;
      const authToken = localStorage.getItem("Authorization");
      const tokenIdx = localStorage.getItem("tokenIdx");
      const ip = localStorage.getItem("client-ip");
      const usrId = localStorage.getItem("usrId");
      if (authToken) {
        config.headers.Authorization = authToken;
      }
      if(tokenIdx)
      {
        config.headers.tokenIdx = tokenIdx;
      }
      if(usrId)
      {
        config.headers.usrId = usrId;
      }
      if (ip) {
        config.headers.ip = ip;
      }
      if (!ip) {
        await axios.get('https://geolocation-db.com/json/')
          .then(res => {
            const clientIp = res.data.IPv4;
            if (clientIp) {
              localStorage.setItem("client-ip", clientIp);
              config.headers.ip = clientIp;
            }
          });
      }
      /*console.log("config = " + JSON.stringify(config));*/
      return config;
    },
    function (error) {
      // 오류 요청을 보내기전 수행할 일
      // ...
      return Promise.reject(error);
    });

  instanceTemp.interceptors.response.use(
    async function(config){
      if(config.headers.getAuthorization() != null)
      {
        if(localStorage.getItem("Authorization") !== config.headers.getAuthorization())
        {
          localStorage.setItem("Authorization",config.headers.getAuthorization()) ;
        }
        else if (localStorage.getItem("Authorization") == null && config.headers.getAuthorization() == null){
          localStorage.removeItem("Authorization");
          localStorage.removeItem("usrId");
          localStorage.removeItem("tokenIdx");
        }
      }
      if(config.headers.get("loginYn") == null)
      {
        localStorage.removeItem("Authorization");
        localStorage.removeItem("usrId");
        localStorage.removeItem("tokenIdx");
      }
      return config;
    }
  );

  return instanceTemp;
}

export class RootStore
{
    instance;
    menuStore;
    loginStore;
    joinStore;
    usrStore;
    authorizationStore;
    menuModalStore;
    postStore;
    postPagingStore;
    chartItemStore;

    constructor() {
        this.instance = instanceCreate();
        this.menuStore = new MenuStore(this);
        this.loginStore = new LoginStore(this);
        this.joinStore = new JoinStore(this);
        this.usrStore = new UsrStore(this);
        this.authorizationStore = new AuthorizationStore(this);
        this.menuModalStore = new MenuModalStore(this);
        this.postStore = new PostStore(this);
        this.postPagingStore = new PostPagingStore(this);
        this.chartItemStore = new ChartItemStore(this);

    }

}