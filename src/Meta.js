import { Helmet } from "react-helmet-async";
import React from "react";

export const Meta = (props) => {

  return (
    <Helmet>
      <title>{props.title}</title>
      <meta name="description" content={props.description}/>
      <meta property="og:type" content={props.type}/>
      <meta property="og:url" content={props.url}/>
      <meta property="og:title" content={props.title}/>
      <meta property="og:image" content={props.image}/>
      <meta property="og:description" content={props.description}/>
      <meta property="og:site_name" content={props.siteNm}/>
      <meta property="og:locale" content="kr"/>
    </Helmet>
    );
};

