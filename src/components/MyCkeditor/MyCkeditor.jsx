import { CKEditor } from "@ckeditor/ckeditor5-react";
import React from "react";
import ClassicEditor from 'ckeditor';
const MyCkeditor = (props)=>
{
  return (
    <CKEditor
      config={props.customConfig }
      editor={ ClassicEditor }
      disabled={props.customDisabled}
      data={props.contents}
      onChange={props.onChange}
    />
  )
}

export default MyCkeditor;