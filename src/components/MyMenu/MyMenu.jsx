import VuiBox from "../VuiBox";
import VuiTypography from "../VuiTypography";
import Card from "@mui/material/Card";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import IconButton from "@mui/material/IconButton";
import React, { useEffect, useMemo, useState } from "react";
import { useStores } from "../../state/context/Context";
import SidenavCollapse from "../../examples/Sidenav/SidenavCollapse";
import ArrowDropDownCircleSharpIcon from "@mui/icons-material/ArrowDropDownCircleSharp";
import { observer } from "mobx-react";
import { v4 as uuidv4 } from "uuid";
import UpdateIcon from '@mui/icons-material/Update';
import List from "@mui/material/List";
import { MenuModal } from "./MenuModal";
import ClearIcon from '@mui/icons-material/Clear';

export const MyMenu =observer((color) =>
{
  const {menuStore, menuModalStore} = useStores();

  useEffect(() => {
    if(menuStore.menus.length === 0)
    {
      if(window.confirm("등록된 메뉴가 없습니다. 등록 하시겠습니까?"))
      {
        handleAddMenuOpen();
      }
    }
  },[])

  const renderMenu = useMemo(() => {
    return menuStore.menus.map((x, i) => (
      <SidenavCollapse
        key={uuidv4()}
        name={x.menuName}
        icon={<ArrowDropDownCircleSharpIcon size="15px" />}
        useComponent="myMenu"
        subMenus={x.subMenus}
        modifyIcon={<UpdateIcon size="15px" />}
        clearIcon={<ClearIcon size="15px" />}
        menuIdx={x.menuIdx}
        index={i}
      />
    ));
  }, [menuStore.menus.map((x) => [...x.subMenus])]);

  const modalStyle = {
    position: 'absolute',
    display: 'inline-grid',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: '#080713',
    border: '2px solid #000',
    borderRadius : '20px',
    boxShadow: 24,
    p: 4,
  };

  const handleAddMenuOpen = () =>
  {
    menuModalStore.setMenuTitle('');
    menuModalStore.setMenuIdx('');
    menuModalStore.setAddOpen(true);
  }

  const handleAddMenuClose = () =>
  {
    menuModalStore.setMenuTitle(null);
    menuModalStore.setMenuIdx(null);
    menuModalStore.setAddOpen(false);
  }

  const handleModifyMenuClose = () =>
  {
    menuModalStore.setMenuTitle(null);
    menuModalStore.setMenuIdx(null);
    menuModalStore.setModifyOpen(false);
  }

  return (
    <VuiBox>
      <VuiTypography compoenet="text" color="white" sx={{mb:'1rem'}}>
        메뉴 관리
      </VuiTypography>
      <Card
        sx={{
          height: "100%",
        }}
      >
        <IconButton sx={{margin:'0 1rem'}} onClick={handleAddMenuOpen}>
          <AddCircleOutlineIcon sx={{color:"darkgray"}} />
        </IconButton>
        <List>
          {renderMenu}
        </List>
      </Card>
      <MenuModal sx={modalStyle} modalTitle="메뉴" modalButton="만들기" open={menuModalStore.addOpen} handleClose={handleAddMenuClose}
                 createFn={(value,menuIdx) => menuStore.createLeftMenu(value,menuIdx)}
      />
      <MenuModal sx={modalStyle} modalTitle="메뉴" modalButton="수정" open={menuModalStore.modifyOpen} handleClose={handleModifyMenuClose}
                 createFn={ (value,menuIdx) => menuStore.modifyLeftMenu(value,menuIdx)}
      />
    </VuiBox>
  );
});

MyMenu.defaultProps =
{
}

MyMenu.propTypes =
{
}