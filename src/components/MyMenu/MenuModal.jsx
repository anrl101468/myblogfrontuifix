import {Box, IconButton, Modal, TextField, Typography} from "@mui/material";
import {Send} from "@mui/icons-material";
import React, { useRef, useState } from "react";
import {observer} from "mobx-react";
import { useStores } from "../../state/context/Context";
import VuiAlert from "../VuiAlert";

export const MenuModal = observer((props) =>
{
    const {menuModalStore} = useStores();

    const [alterOpen,setAlterOpen] = useState(false);
    const handleSubmitClick = () =>
    {
        const title = menuModalStore.menuTitle;
        const menuIdx = menuModalStore.menuIdx;
        if(title.length > 0)
        {
            props.createFn(title,menuIdx);
            menuModalStore.setAddOpen(false);
            menuModalStore.setModifyOpen(false);
            setAlterOpen(false);
        }
        else
        {
            setAlterOpen(true);
        }
    }
    return (
        <Modal
            open={props.open}
            onClose={props.handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={props.sx}>
                <Typography id="modal-modal-title" variant="h6" component="h2" sx={{color:'#ffffff'}}>
                    {props.modalTitle}
                </Typography>
                <TextField name='buttonName'
                           value={menuModalStore.menuTitle}
                           sx={{mt:'1rem'}}
                           onChange={ (event) => menuModalStore.setMenuTitle(event.target.value)}
                />
                <Box display='flex' justifyContent='space-evenly'>
                    <IconButton type="submit" sx={{color:'#ffffff'}} onClick={props.handleClose}>
                        <Send /> 취소
                    </IconButton>
                    <IconButton type="submit" sx={{color:'#ffffff'}} onClick={handleSubmitClick}>
                        <Send /> {props.modalButton}
                    </IconButton>
                </Box>
                {
                    alterOpen == true && (
                        <VuiAlert sx={{fontSize:'0.7em'}}>메뉴명을 입력하세요.</VuiAlert>
                  )
                }
            </Box>
        </Modal>
    )
});