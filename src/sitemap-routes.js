import React from 'react';
import { Route, Switch } from "react-router-dom";

export default (
  <Switch>
    <Route path='/base' />
    <Route path='/user/:id' />
    <Route path='/user/:id/:menuName' />
    <Route path='/user/:id/:menuName/:index' />
    <Route path='/post' />
    <Route path='/authentication/sign-in' />
    <Route path='/authentication/sign-up' />
  </Switch>
);
